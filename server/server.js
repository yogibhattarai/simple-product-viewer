import Express from 'express';
import http from 'http';
import path from 'path';

import "regenerator-runtime/runtime";

import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackConfig from '../webpack.config.js';

import DataManager from './datamanager.js';
const datamanager = new DataManager();

const compiler = webpack(webpackConfig);

const app = Express();
const port = 3007;

app.use(webpackDevMiddleware(compiler, {
    publicPath: "",
    serverSideRender: true
}));

app.use("/gameassets", Express.static(path.resolve(__dirname, "gameassets")));

app.get("/api/shoes", async (req, res)=>{
    try{
        const id = req?.query?.id;

        const allshoes = await datamanager.getShoes(id);
        res.json(allshoes);
    }
    catch(err){}
});

app.use("/", handleRender);

function handleRender(req, res){res.send(renderFullPage());}
function renderFullPage() {
    return `
        <!doctype html>
        <html>
        <head>
            <meta http-equiv="Pragma" content="no-cache" />
            <meta http-equiv="Expires" content="0" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Shoe Vis Example</title>
        </head>
        <body>
            <div id="app-root"></div>

            <script src="/js/shoebundle.js"></script>
        </body>
        </html>
    `
}

const _httpserver = http.Server(app);

_httpserver.listen(port, (err)=>{
    if(err){
        console.log("App Server could not start");
    }
    console.log("Application started @ port "+port);
});