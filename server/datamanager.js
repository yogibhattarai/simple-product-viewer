import fs from 'fs';
import path from 'path';

export default class DataManager{
    constructor(){}

    getShoes(_id){
        try{
            const data = fs.readFileSync(path.resolve(__dirname, "data.json"), {encoding:'utf8', flag:'r'});

            return JSON.parse(data);
        }
        catch(err){
            throw new Error('could not fetch data');
        }
    }
}