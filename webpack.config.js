const path = require("path");
const webpack = require("webpack");

const webpackConfig = {
    target: "web",
    node:{
        global: true
    },
    mode: "development",
    optimization: {
        minimize: false
    },
    devtool: "inline-source-map",
    context: __dirname,
    entry: {
        app: [
            "@babel/polyfill",
            path.resolve(__dirname, "client", "index.js")
        ]
    },
    module: {
        rules: [
            { test: /\.js$/, use:[{loader: "babel-loader"}], exclude: /node_modules/ },
            { test: /\.css$/, use:[{loader: "style-loader"}, {loader: "css-loader"}] },
            { test: /\.scss$/, use:[{loader: "style-loader"}, {loader: "css-loader"}, {loader: "sass-loader"}] },
            { test: /\.(png|jpg|jpeg|gif|svg)$/, use:[{loader: "file-loader"}] },
            { test: /\.(ttf|woff|woff2|eot)$/, use:[{loader: "url-loader"}] }
        ]
    },
    resolve: {
        alias: {
            "@assets": path.resolve(__dirname, "client", "gui", "assets"),
            "@game": path.resolve(__dirname, "client", "game"),
            "@gui": path.resolve(__dirname, "client", "gui"),
            "@scenes": path.resolve(__dirname, "client", "game", "scenes"),
            "@utilities": path.resolve(__dirname, "client", "game", "utilities.js")
        },
        extensions: [".js", ".ts"],
    },
    output: {
        filename: "js/shoebundle.js",
        path: path.resolve(__dirname, "dist")
    },
    plugins: [
        new webpack.DefinePlugin({
          __isBrowser__: "true"
        }),
        new webpack.HotModuleReplacementPlugin()
    ]
};

module.exports = webpackConfig;