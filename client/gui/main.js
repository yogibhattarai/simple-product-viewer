import React from 'react';

import SceneManager from '@game/scenemanager.js';

import '@assets/styles/gui.scss';

import APIUtility from './apiutility.js';

export default class MainGUI extends React.Component{
    constructor(props){
        super(props);

        this.state={
            shoes: [],
            selectedeshoeindex: 0,
            selectedshoecolor: 0,
            viewport:{w: 0, h: 0}
        };

        this.api = new APIUtility();

        this.initGame = this.initGame.bind(this);
    }

    componentDidMount(){
        this.fetchShoes();
    }

    async fetchShoes(){
        const allshoes = await this.api.getShoes();
        this.setState({
            shoes: allshoes,
            viewport: {w: 479, h: window.innerHeight}
        }, ()=>{this.initGame();});
    }

    initGame(){
        this.scene_manager = new SceneManager({
            width: this.state.viewport.w,
            height: this.state.viewport.h,
            shoes: this.state.shoes,
            selectedeshoeindex: this.state.selectedeshoeindex,
            selectedshoecolor: this.state.selectedshoecolor
        });
        this.scene_manager.startEngine();
    }

    updateSelectedShoeIndex(shoeindex){
        this.setState({
            selectedeshoeindex: shoeindex,
            selectedshoecolor: 0
        }, ()=>{
            this.scene_manager.updateProps({
                selectedeshoeindex: this.state.selectedeshoeindex,
                selectedshoecolor: 0
            });
        });
    }

    updateShowColor(colorindex){
        this.setState({
            selectedshoecolor: colorindex
        }, ()=>{
            this.scene_manager.updateProps({
                selectedshoecolor: this.state.selectedshoecolor
            });
        });
    }

    render(){
        return(
            <div className="main-app">
                <div className="shoeselector">
                    <div className="typeselector">
                        {this.state.shoes.map((shoe, s_index)=>{
                            return <div onClick={()=>this.updateSelectedShoeIndex(s_index)} key={`shoeselector-${s_index}`} className="selector-img-container"><img className={s_index===this.state.selectedeshoeindex ? 'selected' : ''} src={`/gameassets${shoe.image}`}/></div>
                        })}
                    </div>
                    <div className="colorselector">
                        {this.state.shoes[this.state.selectedeshoeindex]?.colors?.map((colorobj, c_index)=>{
                            return (
                                <div onClick={()=>this.updateShowColor(c_index)} key={`colorselector-${c_index}`} className="selector-color-container">
                                    <div style={{backgroundColor: colorobj.color, border: (c_index===this.state.selectedshoecolor ? '2px solid #000' : '')}}></div>
                                </div>
                            )
                        })}
                    </div>
                </div>
                <canvas id="render3dcanvas"></canvas>
            </div>
        );
    }
}