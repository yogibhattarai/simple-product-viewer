export default class APIUtility{
    constructor(){}

    request(_method,url){
        const requestOptions = {
            method: _method,
            mode: 'cors',
            cache: 'no-cache',
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            headers:{
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        };
        
        return new Promise((resolve, reject)=>{
            fetch(url, requestOptions)
            .then((response)=>{
                if (response.status !== 200) {
                    reject("API Status Error");
                }
                response.json().then(respdata=>resolve(respdata));
            })
            .catch((err)=>{
                console.log('Fetch Error');
                reject(err);
            });
        });
    }

    async getShoes(shoeid){
        try{
            const shoedata = await this.request("GET", `/api/shoes${shoeid || ''}`);
            return shoedata;
        }
        catch(err){throw new Error('Could Not Fetch Shoe Data');}
    }

}