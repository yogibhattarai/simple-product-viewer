const degToRad = (_deg)=>{
    return _deg * (Math.PI / 180);
}

const radToDeg = (_rad)=>{
    return _rad * (180 / Math.PI);
}

const Random = (max, min)=>{
    if(min){
        return Math.floor(Math.random() * ((max + 1) - min) + min);
    }
    else{
        return Math.floor(Math.random() * Math.floor((max + 1)));
    }
}

export {
    degToRad,
    radToDeg,
    Random
}