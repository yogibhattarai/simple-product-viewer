import {
    WebGLRenderer,
    PCFSoftShadowMap
} from 'three';

import MainScene from '@scenes/mainscene.js';

export default class SceneManager{

    constructor(props){
        this.props = props;

        this.canvas = document.getElementById('render3dcanvas');

        this.renderer = new WebGLRenderer({
            canvas : this.canvas,
            autoClear: true,
            alpha: true
        });
        if(this.props.width && this.props.height){
            this.canvas.width = this.props.width;
            this.canvas.height = this.props.height;
        }
        
        this.renderer.setSize( this.canvas.width, this.canvas.height );
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = PCFSoftShadowMap;

        const _fps = 60;
        this.lockframe = false;

        this.fpsInterval = 1000 / _fps;
        this.then = Date.now();
        this.now, this.elapsed;
        
        this.scene = null;
        this.sceneObj = null;

        this.loop = this.loop.bind(this);
    }

    loop(){
        requestAnimationFrame(this.loop);

        this.now = Date.now();
        this.elapsed = this.now - this.then;

        /* This frame locks */
        if(this.elapsed > this.fpsInterval){
            this.then = this.now - (this.elapsed % this.fpsInterval);
            
            if(this.lockframe && this.scene){
                this.sceneObj.update(this.props);
            }
        }
        /* **************** */

        /* This doesn't */
        if(!this.lockframe && this.scene){
            this.sceneObj.update(this.props);
        }
        /* ************ */
    }

    async startEngine(){
        this.sceneObj = new MainScene(this.renderer, this.canvas, this.props);
        try{
            this.scene = await this.sceneObj.createScene();
        }catch(err){
            console.log("ERROR: -> ", err);
        }

        this.loop();
    }


    updateProps(newprops){
        this.props = {
            ...newprops
        };
    }

}