import {
    Scene,
    PerspectiveCamera,
    MeshPhongMaterial,
    DirectionalLight,
    AmbientLight,
    DoubleSide as THREEDoubleSided,
    TextureLoader
} from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

export default class MainScene{

    constructor(_renderer, _canvas, _props){
        this.renderer = _renderer;
        this.canvas = _canvas;
        this.props = _props;

        this.modelloader = new GLTFLoader();
        this.textureloader = new TextureLoader();

        this.update = this.update.bind(this);
    }

    async createScene(){

        this.scene = new Scene();
        //this.scene.background = new Color(0xffffff);
        this.renderer.setClearColor(0x000000, 0);
        
        this.camera = new PerspectiveCamera(45, this.canvas.width/this.canvas.height, 0.01, 1000);
        this.camera.position.set(0, 1, 1);
        this.camera.lookAt(0, 0, 0);

        const ambientLight = new AmbientLight(0x807a57);
        this.scene.add(ambientLight);

        const directionalLight = new DirectionalLight(0xffffff, 1);
        directionalLight.castShadow = true;
        this.scene.add(directionalLight);

        await this.prepareShoe();

        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.enableDamping = true;
        this.controls.enablePan = false;
        this.controls.maxDistance = 2;
        this.controls.minDistance = 0.5;

        return this.scene;
    }

    update(newprops){
        var updatemodel = false;
        var updatetexture = false;
        if(!isNaN(newprops.selectedeshoeindex) && this.props.selectedeshoeindex !== newprops.selectedeshoeindex) updatemodel = true;
        else if(!isNaN(newprops.selectedshoecolor) && this.props.selectedshoecolor !== newprops.selectedshoecolor) updatetexture = true;

        this.props = {
            ...this.props,
            ...newprops
        };

        if(updatemodel || updatetexture) this.prepareShoe(updatetexture);

        this.controls.update();

        this.renderer.render( this.scene, this.camera );
    }

    async prepareShoe(switchmaterial){
        const previousshoe = this.scene.getObjectByName("currentshoe");
        if(switchmaterial){
            const newshoematerial = this.generateMaterial();
            previousshoe.children[0].material = newshoematerial;
        }
        else{
            if(previousshoe){
                this.scene.remove(previousshoe);
            }

            const modelfileurl = `/gameassets${this.props.shoes[this.props.selectedeshoeindex].model_path}/${this.props.shoes[this.props.selectedeshoeindex].model}`;

            const currentshoe= await this.loadModel(modelfileurl);
            currentshoe.scene.position.set(0, 0, 0);

            const shoematerial = this.generateMaterial();

            currentshoe.scene.children[0].material = shoematerial;
            currentshoe.scene.name = "currentshoe";

            this.scene.add(currentshoe.scene);
        }
    }

    generateMaterial(){
        const texturefolderpath = `/gameassets${this.props.shoes[this.props.selectedeshoeindex].texture_path}`;
        const texturefilepath = `/${this.props.shoes[this.props.selectedeshoeindex].colors[this.props.selectedshoecolor].texture}`
        const shoetexture_url = `${texturefolderpath}${texturefilepath}`;

        const shoetexture_diffuse = this.textureloader.load(shoetexture_url);
        shoetexture_diffuse.flipY = false;
        const materialconfig = {map: shoetexture_diffuse, side: THREEDoubleSided};

        if(this.props.shoes[this.props.selectedeshoeindex].normal){
            const shoenormal_url = `${texturefolderpath}${this.props.shoes[this.props.selectedeshoeindex].normal}`;
            const shoenormal = this.textureloader.load(shoenormal_url);
            shoenormal.flipY = false;
            materialconfig.bumpMap = shoenormal;
            materialconfig.bumpScale = 0.005;
        }
        const shoematerial = new MeshPhongMaterial(materialconfig);

        return shoematerial;
    }

    loadModel(url){
        return new Promise((resolve, reject)=>{
            this.modelloader.load(
                url,
                (gltf)=>{
                    gltf.scene.traverse((node)=>{
                        if(node.isMesh){
                            node.castShadow = true;
                            node.receiveShadow = true;
                        }
                    });
                    resolve(gltf);
                },
                (xhr)=>{
                    console.log((xhr.loaded/xhr.total * 100) + '% loaded');
                },
                (error)=>{
                    console.error('Error:', error);
                    reject(err);
                }
            );
        });
    }

}