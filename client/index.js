import React from 'react';
import ReactDOM from 'react-dom';

import MainGUI from '@gui/main.js';

import '@assets/styles/mainstyle.scss';

ReactDOM.render(
    <MainGUI />,
    document.getElementById('app-root')
);